# refhls-pipc

Trying out PipelineC with a variant of [refhls](https://gist.github.com/skmp/fc3ae9b26f88b37706642cc23b49adca)

With OSS CAD Suite env + PipelineC in path
```
pipelinec refrend_hls.c
```

OR
```
gcc hls_testbench.c refrend_hls.c -DCSIM
```

~
