#pragma once

#if CSIM
#include <stdint.h>
#else
#include "uintN_t.h"
#include "intN_t.h"
#endif

#define u8 uint8_t
#define u16 uint16_t
#define u32 uint32_t
#define f32 float

typedef enum TileCommands {
	OP_VTX,
	OP_ISP,
	OP_TSP,
	OP_TCW,
	OP_RENDERMODE,
	OP_TAG,
	OP_DRAW,
	OP_READ,
	OP_CLEAR,
	OP_DRAWTSP,
	OP_TILEX,
	OP_TILEY,
    OP_CLEARFPU,

	OP_AREA,
} TileCommands;

// PipelineC can't handle declarations
// u32 TileCommand(u32 command, u32 data);