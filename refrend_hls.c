/*******************************************************************************
	What if you generated a PowerVR CLX2 ISP and TSP implementation with HLS?
		Would it fit to the ultra96?

*******************************************************************************/

//#include <math.h>
//#include <memory.h>

#define bool char
#define true 1
#define false 0

#include "refrend_hls.h"

#define RM_OPAQUE 0
#define RM_PUNCHTHROUGH 1
#define RM_TRANSLUCENT 2
#define RM_MODIFIER 3

#define RenderMode u32

/*
typedef enum {
    RM_OPAQUE,
    RM_PUNCHTHROUGH,
    RM_TRANSLUCENT,
    RM_MODIFIER,
    RM_COUNT
} RenderMode;
*/

int32_t area_left, area_top, area_right, area_bottom;

typedef struct ISP_TSP
{
		u32 Reserved    : 20;
		u32 DCalcCtrl   : 1;
		u32 CacheBypass : 1;
		u32 UV_16b      : 1; //In TA they are replaced
		u32 Gouraud     : 1; //by the ones on PCW
		u32 Offset      : 1; //
		u32 Texture     : 1; // -- up to here --
		u32 ZWriteDis   : 1;
		u32 CullMode    : 2;
		u32 DepthMode   : 3;
} ISP_TSP;

typedef struct ISP_Modvol
{
	u32 id         : 26;
	u32 VolumeLast : 1;
	u32 CullMode   : 2;
	u32 DepthMode  : 3;
} ISP_Modvol;


//// END ISP/TSP Instruction Word


//// TSP Instruction Word

typedef struct TSP
{
	u32 TexV        : 3;
	u32 TexU        : 3;
	u32 ShadInstr   : 2;
	u32 MipMapD     : 4;
	u32 SupSample   : 1;
	u32 FilterMode  : 2;
	u32 ClampV      : 1;
	u32 ClampU      : 1;
	u32 FlipV       : 1;
	u32 FlipU       : 1;
	u32 IgnoreTexA  : 1;
	u32 UseAlpha    : 1;
	u32 ColorClamp  : 1;
	u32 FogCtrl     : 2;
	u32 DstSelect   : 1; // Secondary Accum
	u32 SrcSelect   : 1; // Primary Accum
	u32 DstInstr    : 3;
	u32 SrcInstr    : 3;
} TSP;


//// END TSP Instruction Word


/// Texture Control Word
typedef struct TCW
{
	u32 TexAddr   :21;
	u32 Reserved  : 4;
	u32 StrideSel : 1;
	u32 ScanOrder : 1;
	u32 PixelFmt  : 3;
	u32 VQ_Comp   : 1;
	u32 MipMapped : 1;
} TCW;


//Vertex storage types
typedef struct Vertex
{
	float x,y,z;

	u32 col;
	u32 spc;

	float u,v;

	// Two volumes format
	u32 col1;
	u32 spc1;

	float u1,v1;
} Vertex;

// Some global state
Vertex v1;
Vertex v2;
Vertex v3;
Vertex v4;


// interpolation helper
typedef struct PlaneStepper3
{
    float ddx, ddy;
    float c;
} PlaneStepper3;

PlaneStepper3 Setup(float v1_a, float v2_a, float v3_a)
{
	PlaneStepper3 rv;

	float Aa = ((v3_a - v1_a) * (v2.y - v1.y) - (v2_a - v1_a) * (v3.y - v1.y));
	float Ba = ((v3.x - v1.x) * (v2_a - v1_a) - (v2.x - v1.x) * (v3_a - v1_a));

	float C = ((v2.x - v1.x) * (v3.y - v1.y) - (v3.x - v1.x) * (v2.y - v1.y));

	rv.ddx = -Aa / C;
	rv.ddy = -Ba / C;

	rv.c = (v1_a - rv.ddx * v1.x - rv.ddy * v1.y);

	return rv;
}

float Ip2(const PlaneStepper3 ps, float x, float y)
{
	return x * ps.ddx + y * ps.ddy + ps.c;
}

float Ip(const PlaneStepper3 ps, float x, float y, float W)
{
	return Ip2(ps, x, y) * W;
}

////// more interpolation helpers /////

// color helper
#define SEL_COL(col, i) ( ((col) >> (8 * i)) & 255 )

typedef struct IPs3
{
    PlaneStepper3 U;
    PlaneStepper3 V;
    PlaneStepper3 Col[4];
    PlaneStepper3 Ofs[4];
} IPs3;

IPs3 Setup3(u8 TexU, u8 TexV, bool pp_Gourand)
{
	u32 w = 0x8;
	w = w << TexU;

	u32 h = 0x8;
	h = h << TexV;

	IPs3 rv;

	rv.U = Setup(v1.u * (float)w * v1.z, v2.u * (float)w * v2.z, v3.u * (float)w * v3.z);
	rv.V = Setup(v1.v * (float)h * v1.z, v2.v * (float)h * v2.z, v3.v * (float)h * v3.z);
	u32 i;
	if (pp_Gourand) {
		for (i = 0; i < 4; i = i + 1)
			rv.Col[i] = Setup((float)SEL_COL(v1.col, i)  * v1.z, (float)SEL_COL(v2.col, i)  * v2.z, (float)SEL_COL(v3.col, i)  * v3.z);

		for (i = 0; i < 4; i = i + 1)
			rv.Ofs[i] = Setup((float)SEL_COL(v1.spc, i) * v1.z, (float)SEL_COL(v2.spc, i) * v2.z, (float)SEL_COL(v3.spc, i) * v3.z);
	} else {
		for (i = 0; i < 4; i = i + 1)
			rv.Col[i] = Setup((float)SEL_COL(v3.col, i)  * v1.z, (float)SEL_COL(v3.col, i)  * v2.z, (float)SEL_COL(v3.col, i)  * v3.z);

		for (i = 0; i < 4; i = i + 1)
			rv.Ofs[i] = Setup((float)SEL_COL(v3.spc, i) * v1.z, (float)SEL_COL(v3.spc, i) * v2.z, (float)SEL_COL(v3.spc, i) * v3.z);
	}

	return rv;
}


typedef struct DrawParameters
{
	ISP_TSP isp;
	TSP tsp;
	TCW tcw;
	TSP tsp2;
	TSP tcw2;
} DrawParameters;

typedef struct FpuEntry {
	IPs3 IPs;
	DrawParameters params;
} FpuEntry;

//
//typedef u16 parameter_tag_t;

#define parameter_tag_t u16

////// DEFINES ///////

#define MAX_RENDER_WIDTH 32
#define MAX_RENDER_HEIGHT 32
#define MAX_RENDER_PIXELS (MAX_RENDER_WIDTH * MAX_RENDER_HEIGHT)

#define STRIDE_PIXEL_OFFSET MAX_RENDER_WIDTH

#define PARAM_BUFFER_PIXEL_OFFSET   0
#define DEPTH1_BUFFER_PIXEL_OFFSET  (MAX_RENDER_PIXELS*1)
#define DEPTH2_BUFFER_PIXEL_OFFSET  (MAX_RENDER_PIXELS*2)
#define STENCIL_BUFFER_PIXEL_OFFSET (MAX_RENDER_PIXELS*3)
#define ACCUM1_BUFFER_PIXEL_OFFSET  (MAX_RENDER_PIXELS*4)
#define ACCUM2_BUFFER_PIXEL_OFFSET  (MAX_RENDER_PIXELS*5)

#define TAG_INVALID 1


///// GLOBAL STATE /////

typedef struct pb_t { u16 b[MAX_RENDER_PIXELS]; } pb_t;
typedef struct db_t { f32 b[MAX_RENDER_PIXELS]; } db_t;
typedef struct sb_t { u8  b[MAX_RENDER_PIXELS]; } sb_t;
typedef struct ab_t { u32 b[MAX_RENDER_PIXELS]; } ab_t;

// Render buffers
typedef struct all_bufs {
	pb_t ParamBuffer;
	db_t DepthBuffer1;
	db_t DepthBuffer2;
	sb_t StencilBuffer;
	ab_t AccumBuffer1;
	ab_t AccumBuffer2;
} all_bufs;

typedef struct bu_rv {
	bool rv;
	ab_t AccumBuffer1;
	ab_t AccumBuffer2;
} bu_rv;

typedef struct tsp_rv {
	pb_t ParamBuffer;
	sb_t  StencilBuffer;
	bu_rv bu;
} tsp_rv;

typedef struct isp_rv {
	pb_t ParamBuffer;
	sb_t StencilBuffer;
	db_t DepthBuffer1;
	db_t DepthBuffer2;
} isp_rv;

#define pb rvb.ParamBuffer.b[CurrentPixel]
#define sb rvb.StencilBuffer.b[CurrentPixel]
#define zb rvb.DepthBuffer1.b[CurrentPixel]
#define zb2 rvb.DepthBuffer2.b[CurrentPixel]
#define ab1 rvb.AccumBuffer1.b[CurrentPixel]
#define ab2 rvb.AccumBuffer2.b[CurrentPixel]

// Tile render state
u32 TileX;
u32 TileY;

// ISP/TSP render state
RenderMode render_mode;

parameter_tag_t CurrentTag;

FpuEntry CurrentFpuEntry;

// FPU render state

FpuEntry FpuEntries[1 * 1024];
u16 LastFpuEntry;

////// Rendering Helpers //////
static float min(float a, float b) {
	return a > b ? b : a;
}

static float max(float a, float b) {
	return a < b ? b : a;
}

static float fabsf(float v) {
	return v < 0.0f ? -v : v;
}

static bool isnan2(float v) {
	return !(v == v);
}

static float mmin(float a, float b, float c, float d)
{
    float rv = min(a, b);
    rv = min(c, rv);
    return max(d, rv);
}

static float mmax(float a, float b, float c, float d)
{
    float rv = max(a, b);
    rv = max(c, rv);
    return min(d, rv);
}


////// TSP //////
u32 ColorCombiner(bool pp_Texture, bool pp_Offset, u32 pp_ShadInstr, u32 base, u32 textel, u32 offset) {

	u32 rv = base;
	if (pp_Texture)
	{
		if (pp_ShadInstr == 0)
		{
			//color.rgb = texcol.rgb;
			//color.a = texcol.a;

			rv = textel;
		}
		else if (pp_ShadInstr == 1)
		{
			u32 i;
			//color.rgb *= texcol.rgb;
			//color.a = texcol.a;
			for (i = 0; i < 3; i = i + 1)
			{
				rv |= (SEL_COL(textel, i) * SEL_COL(base, i) / 256) << (i * 8);
			}

			rv |=  SEL_COL(textel, 3) << 24;
		}
		else if (pp_ShadInstr == 2)
		{
			//color.rgb=mix(color.rgb,texcol.rgb,texcol.a);
			u8 tb = SEL_COL(textel, 3);
			u8 cb = 255 - tb;
			u32 i;
			for (i = 0; i < 3; i = i + 1)
			{
				rv |= ((SEL_COL(textel, i) * tb + SEL_COL(base, i) * cb) / 256) << (i * 8);
			}

			rv |=  SEL_COL(base, 3) << 24;
		}
		else if (pp_ShadInstr == 3)
		{
			//color*=texcol
			u32 i;
			for (i = 0; i < 4; i = i + 1)
			{
				rv |= (SEL_COL(textel, i) * SEL_COL(base, i) / 256) << (i * 8);
			}
		}

		if (pp_Offset) {
			// mix only color, saturate
			u32 i;
			for (i = 0; i < 3; i = i + 1)
			{
				rv |= (SEL_COL(rv, i) * SEL_COL(offset, i) / 256) << (i * 8);
			}
		}
	}

	return rv;
}

static u32 InterpolateBase(bool pp_CheapShadows, bool pp_UseAlpha, const PlaneStepper3 Col[4], float x, float y, float W, u32 stencil) {
	u32 rv = 0;
	float mult = 256.0f;

	if (pp_CheapShadows) {
		if (stencil & 1) {
			mult = 128.0f;
		}
	}

	rv |= (u8)(Ip(Col[2], x, y, W) * mult / 256.0f) << 0;   // FIXME: why is input in RGBA instead of BGRA here?
	rv |= (u8)(Ip(Col[1], x, y, W) * mult / 256.0f) << 8;
	rv |= (u8)(Ip(Col[0], x, y, W) * mult / 256.0f) << 16;
	rv |= (u8)(Ip(Col[3], x, y, W) * mult / 256.0f) << 24;

	if (!pp_UseAlpha)
	{
		rv |= 255 << 24;
	}

	//rv = 0xFFFFFFFF;
	return rv;
}

static u32 BlendCoefs(u32 pp_AlphaInst, bool srcOther, u32 src, u32 dst) {
	u32 rv;

	u32 swv = pp_AlphaInst>>1;

	u32 i = 0;

	if (swv == 0) {
		rv = 0;
	} else if (swv == 1) {
		rv = srcOther? src : dst;
	} else if (swv == 1) {
		for (i = 0; i < 4; i = i + 1) rv |= SEL_COL(src, 3) << (i * 8);
	} else {
		for (i = 0; i < 4; i = i + 1) rv |= SEL_COL(dst, 3) << (i * 8);
	}

	if (pp_AlphaInst & 1) {
		rv = rv ^ 0xFFffFFff;
	}

	return rv;
}

static bu_rv BlendingUnit(bool pp_AlphaTest, u32 pp_SrcSel, u32 pp_DstSel, u32 pp_SrcInst, u32 pp_DstInst, u32 col, bu_rv rvb, u16 CurrentPixel)
{
	u32 rv = 0;
	u32 src = pp_SrcSel ? ab2 : col;
	u32 dst = pp_DstSel ? ab2 : ab1;

	u32 src_blend = BlendCoefs(pp_SrcInst, false, src, dst);
	u32 dst_blend = BlendCoefs(pp_DstInst, true, src, dst);

	u32 j = 0;
	for (j = 0; j < 4; j = j + 1)
	{
		rv |= ((SEL_COL(src, j) * SEL_COL(src_blend, j) + SEL_COL(dst, j) * SEL_COL(dst_blend, j)) >> 8) << (j * 8);
	}

	bool passedAlphaTest = false;

	if (!pp_AlphaTest | SEL_COL(src, 3) >= 128)
	{
		passedAlphaTest = true;
		if (pp_DstSel)
			ab2 = rv;
		else
			ab1 = rv;
	}

	rvb.rv = passedAlphaTest;

	return rvb;
}

static tsp_rv PixelFlush_tsp(float x, float y, float invW, tsp_rv rvb, u16 CurrentPixel)
{
   float W = 1.0f / invW;

   pb = pb | TAG_INVALID;

   u32 base = 0, textel = 0, offs = 0;

   base = InterpolateBase(false, CurrentFpuEntry.params.tsp.UseAlpha, CurrentFpuEntry.IPs.Col, x, y, W, sb);

   if (CurrentFpuEntry.params.isp.Texture) {
	   float u = Ip(CurrentFpuEntry.IPs.U, x, y, W);
	   float v = Ip(CurrentFpuEntry.IPs.V, x, y, W);

	   textel = 0xFFFFFFFF;//u * v * 65535*65535;//entry->textureFetch(&entry->texture, u, v);
	   if (CurrentFpuEntry.params.isp.Offset) {
		   offs = InterpolateBase(false, CurrentFpuEntry.params.tsp.UseAlpha, CurrentFpuEntry.IPs.Ofs, x, y, W, sb);
	   }
   }

   u32 col = ColorCombiner(CurrentFpuEntry.params.isp.Texture, CurrentFpuEntry.params.isp.Offset, CurrentFpuEntry.params.tsp.ShadInstr, base, textel, offs);

   //col = FogUnit<pp_Offset, pp_ColorClamp, pp_FogCtrl>(col, 1/W, offs.a);

	rvb.bu = BlendingUnit(render_mode == RM_PUNCHTHROUGH, CurrentFpuEntry.params.tsp.SrcSelect, CurrentFpuEntry.params.tsp.DstSelect, CurrentFpuEntry.params.tsp.SrcInstr, CurrentFpuEntry.params.tsp.DstInstr, col, rvb.bu, CurrentPixel);
   //return entry->blendingUnit(cb, col);
   //*cb = col;

   return rvb;
}

////// ISP //////
static isp_rv PixelFlush_isp(RenderMode render_mode, u32 depth_mode, float x, float y, float invW, isp_rv rvb, u16 CurrentPixel)
{
	u32 mode = depth_mode;

	if (render_mode == RM_PUNCHTHROUGH)
		mode = 6; // TODO: FIXME
	else if (render_mode == RM_TRANSLUCENT)
		mode = 3; // TODO: FIXME
	else if (render_mode == RM_MODIFIER)
		mode = 6;

/*
	switch(mode) {
		// never
		case 0: return; break;
		// less
		case 1: if (invW >= zb) return; break;
		// equal
		case 2: if (invW != zb) return; break;
		// less or equal
		case 3: if (invW > zb) return; break;
		// greater
		case 4: if (invW <= zb) return; break;
		// not equal
		case 5: if (invW == zb) return; break;
		// greater or equal
		case 6: if (invW < zb) return; break;
		// always
		case 7: break;
	}*/

	// OPAQ
	if (render_mode == RM_OPAQUE) {
		// Z pre-pass only

		zb = invW;
		pb = CurrentTag;
	} else if (render_mode == RM_MODIFIER) {
		// Flip on Z pass

		sb = sb ^ 0b10;
	} else if (render_mode == RM_PUNCHTHROUGH) {
		// PT
		// Z + TSP synchronized for alpha test

		//if (AlphaTest_tsp(backend, x, y, pb, invW, tag))
		{
			zb = invW;
			pb = CurrentTag;
		}
	} else if (render_mode == RM_TRANSLUCENT) {
		// Layer Peeling. zb2 holds the reference depth, zb is used to find closest to reference
		bool DoWrite = true;
		
		if (invW < zb2)
			DoWrite = false;

		if (invW == zb | invW == zb2) {
			parameter_tag_t tagExisting = pb;

			if (tagExisting & TAG_INVALID)
			{
				if (CurrentTag< tagExisting)
					DoWrite = false;
			}
			else
			{
				if (CurrentTag >= tagExisting)
					DoWrite = false;
			}
		}

		//backend->PixelsDrawn++;

		if (DoWrite) {
			zb = invW;
			pb = CurrentTag;
		}
	}

	return rvb;
}

//typedef union u2fu { float f; u32 u; } u2fu;

// fix me
float u2f(u32 u) {
	/*
	u2fu rv;

	rv.u = u;

	return rv.f;
	*/

	return (float)u;
}

// fix me
u32 f2u(float f) {
	return (u32)f;
}

// luv the name
all_bufs rvb;

#pragma MAIN TileCommand
u32 TileCommand(u32 command, u32 data) {
	u32 rv = 0;
	
	u8 op = command >> 24;
	if (op == OP_VTX) {
		u8 vtx = (command >> 16) & 0xFF;
		u8 idx = (command >> 8) & 0xFF;
		Vertex v; // hmm

		if (vtx == 0) { v = v1; }
		else if (vtx == 1) { v = v2; }
		else if (vtx == 2) { v = v3; }
		else { v = v4; }

		if (idx == 0) 		{ v.x = u2f(data); }
		else if (idx == 1 ) { v.y = u2f(data); }
		else if (idx == 2 ) { v.z = u2f(data); }
		else if (idx == 3 ) { v.col = data; }
		else if (idx == 4 ) { v.spc = data; }
		else if (idx == 5 ) { v.u = u2f(data); }
		else if (idx == 6 ) { v.v = u2f(data); }
		else if (idx == 7 ) { v.col1 = data; }
		else if (idx == 8 ) { v.spc1 = data; }
		else if (idx == 9 ) { v.u1 = u2f(data); }
		else { v.v1 = u2f(data); }

		if (vtx == 0) { v1 = v; }
		else if (vtx == 1) { v2 = v; }
		else if (vtx == 2) { v3 = v; }
		else { v4 = v; }

		rv = 1;
	} else if (op == OP_DRAW) {
        //Plane equation

#define FLUSH_NAN(a) isnan2(a) ? 0 : a

        const float Y1 = FLUSH_NAN(v1.y);
        const float Y2 = FLUSH_NAN(v2.y);
        const float Y3 = FLUSH_NAN(v3.y);


        const float X1 = FLUSH_NAN(v1.x);
        const float X2 = FLUSH_NAN(v2.x);
        const float X3 = FLUSH_NAN(v3.x);

        float sgn = 1;

		bool Culled = false;
        // cull
        {
            //area: (X1-X3)*(Y2-Y3)-(Y1-Y3)*(X2-X3)
            float area = ((X1 - X3) * (Y2 - Y3) - (Y1 - Y3) * (X2 - X3));

            if (area > 0.0f)
                sgn = -1;

            if (1 != 0) {
                float abs_area = fabsf(area);

                if (abs_area < 0.001f)
                    Culled = true;
            }
        }

		if (!Culled) {
			// Bounding rectangle
			int32_t minx = (int32_t)mmin(X1, X2, X3, (float)area_left);
			int32_t miny = (int32_t)mmin(Y1, Y2, Y3, (float)area_top);

			int32_t spanx = (int32_t)mmax(X1+1.0f, X2+1.0f, X3+1.0f, (float)area_right) - minx + 1;
			int32_t spany = (int32_t)mmax(Y1+1.0f, Y2+1.0f, Y3+1.0f, (float)area_bottom) - miny + 1;

			//Inside scissor area?
			if (spanx < 0 | spany < 0)
				rv = 0;
			else {
				// Half-edge constants
				const float DX12 = sgn * (X1 - X2);
				const float DX23 = sgn * (X2 - X3);
				const float DX31 = sgn * (X3 - X1);
				const float DX41 = 0;

				const float DY12 = sgn * (Y1 - Y2);
				const float DY23 = sgn * (Y2 - Y3);
				const float DY31 = sgn * (Y3 - Y1);
				const float DY41 = 0;

				float C1 = DY12 * X1 - DX12 * Y1;
				float C2 = DY23 * X2 - DX23 * Y2;
				float C3 = DY31 * X3 - DX31 * Y3;
				float C4 = 1;



				u16 CurrentPixelY = (miny - area_top) * STRIDE_PIXEL_OFFSET + (minx - area_left);

				PlaneStepper3 Z;
				Z = Setup(v1.z, v2.z, v3.z);

				CurrentFpuEntry.IPs = Setup3(CurrentFpuEntry.params.tsp.TexU, CurrentFpuEntry.params.tsp.TexV, CurrentFpuEntry.params.isp.Gouraud);
				FpuEntries[LastFpuEntry] = CurrentFpuEntry;

				CurrentTag = LastFpuEntry << 1;

				LastFpuEntry += 1;

				float halfpixel = 0.5f;
				float y_ps = (float)miny + halfpixel;
				float minx_ps = (float)minx + halfpixel;

				///auto pixelFlush = pixelPipeline->GetIsp(render_mode, params->isp);

				// Loop through pixels
				int32_t y;
				for (y = 2/*spany*/; y > 0; y = y - 1)
				{
					u16 CurrentPixel = CurrentPixelY;
					float x_ps = minx_ps;
					int32_t x;
					for (x = 2/*spanx*/; x > 0; x = x - 1)
					{
						float Xhs12 = C1 + DX12 * y_ps - DY12 * x_ps;
						float Xhs23 = C2 + DX23 * y_ps - DY23 * x_ps;
						float Xhs31 = C3 + DX31 * y_ps - DY31 * x_ps;
						float Xhs41 = C4 + DX41 * y_ps - DY41 * x_ps;

						bool inTriangle = Xhs12 >= 0.0f & Xhs23 >= 0.0f & Xhs31 >= 0.0f & Xhs41 >= 0.0f;

						if (inTriangle)
						{
							float invW = Ip2(Z, x_ps, y_ps);
							isp_rv psin;
							psin.DepthBuffer1 = rvb.DepthBuffer1;
							psin.DepthBuffer2 = rvb.DepthBuffer2;
							psin.ParamBuffer = rvb.ParamBuffer;
							psin.StencilBuffer = rvb.StencilBuffer;

							isp_rv psrv = PixelFlush_isp(render_mode, CurrentFpuEntry.params.isp.DepthMode, x_ps, y_ps, invW, psin, CurrentPixel);

							rvb.DepthBuffer1 = psin.DepthBuffer1;
							rvb.DepthBuffer2 = psin.DepthBuffer2;
							rvb.ParamBuffer = psin.ParamBuffer;
							rvb.StencilBuffer = psin.StencilBuffer;
						}

						CurrentPixel = CurrentPixel + 1;
						x_ps = x_ps + 1.0f;
					}
				//next_y:
					CurrentPixelY += STRIDE_PIXEL_OFFSET;
					y_ps = y_ps + 1.0f;
				}

				rv = 1;
			}
		}
	} else if (op == OP_READ) {
		u8 buffer = (command >> 16) & 0xFF;
		u16 index = command & 1023;
		if (buffer == 0)
			rv = rvb.ParamBuffer.b[index];
		else if (buffer == 1)
			rv = f2u(rvb.DepthBuffer1.b[index]);
		else if (buffer == 2)
			rv = f2u(rvb.DepthBuffer2.b[index]);
		else if (buffer == 3)
			rv = rvb.StencilBuffer.b[index];
		else if (buffer == 4)
			rv = rvb.AccumBuffer1.b[index];
		else if (buffer == 5)
			rv = rvb.AccumBuffer2.b[index];
	} else if (op == OP_ISP) {
		//CurrentFpuEntry.params.isp = data;
	} else if (op == OP_TSP) {
		//CurrentFpuEntry.params.tsp = data;
	} else if (op == OP_TCW) {
		//CurrentFpuEntry.params.tcw = data;
	} else if (op == OP_RENDERMODE) {
		render_mode = (RenderMode)data;
	} else if (op == OP_TAG) {
		CurrentTag = data;
	} else if (op == OP_CLEAR) {
		int32_t i;
		#define CLRB(name) for (i = 0; i < 2/*1024*/; i = i + 1) rvb.name.b[i] = 0;
		CLRB(ParamBuffer)
		CLRB(DepthBuffer1)
		CLRB(DepthBuffer2)
		CLRB(StencilBuffer)
		CLRB(AccumBuffer1)
		CLRB(AccumBuffer2)
	} else if (op == OP_DRAWTSP) {
        float halfpixel =  0.5f;
		int32_t y;
        for (y = 0; y < 2/*32*/; y = y + 1) {
			int32_t x;
            for (x = 0; x < 2/*32*/; x = x + 1) {
            	u16 CurrentPixel = (y << 5) | x;
            	parameter_tag_t tag = rvb.ParamBuffer.b[CurrentPixel];
            	if (!(tag & TAG_INVALID)) {
            		if (CurrentTag != tag) {
						CurrentTag = tag;
						CurrentFpuEntry = FpuEntries[tag >> 1];
					}

					if (CurrentTag != 0) {
						tsp_rv tsprv;
						tsprv.ParamBuffer = rvb.ParamBuffer;
						tsprv.StencilBuffer = rvb.StencilBuffer;
						tsprv.bu.AccumBuffer1 = rvb.AccumBuffer1;
						tsprv.bu.AccumBuffer2 = rvb.AccumBuffer2;

						tsprv = PixelFlush_tsp((float)(TileX + x) + halfpixel, (float)(TileY + y) + halfpixel, rvb.DepthBuffer1.b[CurrentPixel], tsprv, CurrentPixel);

						rvb.ParamBuffer = tsprv.ParamBuffer;
						rvb.StencilBuffer = tsprv.StencilBuffer;
						rvb.AccumBuffer1 = tsprv.bu.AccumBuffer1;
						rvb.AccumBuffer2 = tsprv.bu.AccumBuffer2;
					}
						
            	}
            }
        }
	} else if (op == OP_TILEX) {
		TileX = data;
	} else if (op == OP_TILEY) {
		TileY = data;
	} else if (op == OP_CLEARFPU) {
        LastFpuEntry = 1;
    } else if (op == OP_AREA) {
		u8 item = (command >> 16) & 0xFF;
		if (item == 0) {
			area_left = data;
		} else if (item == 1) {
			area_top = data;
		} else if (item == 2) {
			area_right = data;
		} else if (item == 3) {
			area_bottom = data;
		}
	}

	return rv;
}
