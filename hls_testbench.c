#include "refrend_hls.h"

#include <stdio.h>

u32 TileCommand(u32 command, u32 data);

int main() 
{
    int err =0; 
    
    // draw a few tiles
    for (int kk = 0; kk < 20; kk++) {
        TileCommand(OP_CLEARFPU << 24, 0);
        TileCommand((OP_AREA << 24) | ( 0 << 16), 0);
        TileCommand((OP_AREA << 24) | ( 1 << 16), 0);

        TileCommand((OP_AREA << 24) | ( 2 << 16), 31);
        TileCommand((OP_AREA << 24) | ( 3 << 16), 31);

        TileCommand(OP_CLEAR << 24, 0);

        TileCommand(OP_ISP << 24, (7 << 29) | ( 1 << 23));
        TileCommand(OP_TSP << 24, 1 << 29);
        TileCommand(OP_TCW << 24, 0);

        TileCommand(OP_RENDERMODE << 24, 0); // RM_OPAQUE

        TileCommand((OP_VTX << 24) | ( 0 << 16) | (0 << 8) | 0, 0); // X
        TileCommand((OP_VTX << 24) | ( 0 << 16) | (1 << 8) | 0, 0); // Y
        TileCommand((OP_VTX << 24) | ( 0 << 16) | (2 << 8) | 0, 0x3f800000); // Z
        TileCommand((OP_VTX << 24) | ( 0 << 16) | (3 << 8) | 0, 0x8000a000); // Col

        TileCommand((OP_VTX << 24) | ( 1 << 16) | (0 << 8) | 0, 0); // X
        TileCommand((OP_VTX << 24) | ( 1 << 16) | (1 << 8) | 0, 0x42000000); // Y - 32
        TileCommand((OP_VTX << 24) | ( 1 << 16) | (2 << 8) | 0, 0x3f800000); // Z
        TileCommand((OP_VTX << 24) | ( 1 << 16) | (3 << 8) | 0, 0); // Col

        TileCommand((OP_VTX << 24) | ( 2 << 16) | (0 << 8) | 0, 0x42000000); // X - 32
        TileCommand((OP_VTX << 24) | ( 2 << 16) | (1 << 8) | 0, 0x42000000); // Y - 32
        TileCommand((OP_VTX << 24) | ( 2 << 16) | (2 << 8) | 0, 0x3f800000); // Z
        TileCommand((OP_VTX << 24) | ( 2 << 16) | (3 << 8) | 0, 0x008000a0); // Col

        // background poly
        TileCommand(OP_DRAW << 24, 0);

        // non-bg triangle
        TileCommand(OP_DRAW << 24, 0);

        TileCommand((OP_VTX << 24) | ( 0 << 16) | (0 << 8) | 0, 0); // X
        TileCommand((OP_VTX << 24) | ( 0 << 16) | (1 << 8) | 0, 0x40e00000); // Y
        TileCommand((OP_VTX << 24) | ( 0 << 16) | (2 << 8) | 0, 0x3f800000); // Z
        TileCommand((OP_VTX << 24) | ( 0 << 16) | (3 << 8) | 0, 0x10101080); // Col

        TileCommand((OP_VTX << 24) | ( 1 << 16) | (0 << 8) | 0, 0x40e00000); // X
        TileCommand((OP_VTX << 24) | ( 1 << 16) | (1 << 8) | 0, 0x41700000); // Y
        TileCommand((OP_VTX << 24) | ( 1 << 16) | (2 << 8) | 0, 0x41700000); // Z
        TileCommand((OP_VTX << 24) | ( 1 << 16) | (3 << 8) | 0, 0x80801020); // Col

        TileCommand((OP_VTX << 24) | ( 2 << 16) | (0 << 8) | 0, 0x40e00000); // X
        TileCommand((OP_VTX << 24) | ( 2 << 16) | (1 << 8) | 0, 0x40e00000); // Y
        TileCommand((OP_VTX << 24) | ( 2 << 16) | (2 << 8) | 0, 0x3f800000); // Z
        TileCommand((OP_VTX << 24) | ( 2 << 16) | (3 << 8) | 0, 0x10108010); // Col

        // another triangle
        TileCommand(OP_DRAW << 24, 0);

        TileCommand(OP_DRAWTSP << 24, 0);
    }
    printf("P1\n32 32\n");
	for (int i = 0; i < 32; i++) {
		for (int j = 0; j < 32; j++) {
			printf("%d ", TileCommand((OP_READ << 24) | (0 << 16) | (i* 32 + j), 0));
		}
		printf("\n");
	}

    FILE* fptr = fopen("col.tga", "wb");
    putc(0,fptr);
    putc(0,fptr);
    putc(2,fptr);                         /* uncompressed RGB */
    putc(0,fptr); putc(0,fptr);
    putc(0,fptr); putc(0,fptr);
    putc(0,fptr);
    putc(0,fptr); putc(0,fptr);           /* X origin */
    putc(0,fptr); putc(0,fptr);           /* y origin */
    putc((32 & 0x00FF),fptr);
    putc((32 & 0xFF00) / 256,fptr);
    putc((32 & 0x00FF),fptr);
    putc((32 & 0xFF00) / 256,fptr);
    putc(32,fptr);                        /* 24 bit bitmap */
    putc(0,fptr);

    for (int i = 0; i < 32; i++) {
    	for (int j = 0; j < 32; j++) {
            u32 col = TileCommand((OP_READ << 24) | (4 << 16) | (i* 32 + j), 0);
    		putc(col >> 24, fptr);
            putc(col >> 16, fptr);
            putc(col >> 8, fptr);
            putc(col >> 0, fptr);
    	}
    }
    fclose(fptr);


}